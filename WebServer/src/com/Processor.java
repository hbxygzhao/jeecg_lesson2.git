package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
public class Processor extends Thread{
	private Socket socket;
	private InputStream in;
	private PrintStream out;
	private final static String wroot="D:\\zhao\\Workspaces\\MyEclipse 2017 CI\\mes";
	public Processor(Socket socket)
	{
		this.socket=socket;
		try {
			in=socket.getInputStream();
			out=new PrintStream(socket.getOutputStream());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	public void run()
	{
		String filename=parse(in);
		SendFile(filename);	
	}
	//根据输出流，得到访问的资源，返回资源名称
	public String  parse(InputStream in)
	{   //字节流转为字符流，提高效率
		BufferedReader  br=new BufferedReader(new InputStreamReader(in));
		String filename=null;
		try {			
			String HttpMessage=br.readLine();
			//正确数组分三部分
			String[] context=HttpMessage.split(" ");
			if(context.length!=3)
			{
				SendErrorMessage(400, "client query error!");				
			}
			System.out.println("context code="+context[0]+",filename="+context[1]+",http-version="+context[2]);
			filename=context[1];
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return filename;
		
	}
	//返回错误信息
	public void SendErrorMessage(int ErrorCode,String ErrorMessage)
	{
		System.out.println("http 1.0-"+ErrorCode+"-"+ErrorMessage);
		System.out.println("content-type:text/html");
		System.out.println();
		System.out.println("<html>");
		System.out.println("<title>Error Message");
		System.out.println("</title>");
		System.out.println("<body>");
		System.out.println("<h1>ErrorCode:"+ErrorCode+";ErrorMessage"+ErrorMessage+"</h1>");
		System.out.println("</body>");
		System.out.println("</html>");
		out.flush();
		out.close();
		try {
			in.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			System.in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//返回资源
	public void SendFile(String Filename)
	{
		File file=new File(Processor.wroot+Filename);
		if(!file.exists())
		{
			this.SendErrorMessage(404, "File not found!");		
			return;
		}
		try {
			InputStream in=new FileInputStream(file);
			byte context[]=new byte[(int)file.length()];
			in.read(context);
			System.out.println("http 1.0 200 queryfile");
			System.out.println("content-length:"+context.length);
			System.out.println();
			out.write(context);
			out.flush();
		    out.close();
			in.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
}
